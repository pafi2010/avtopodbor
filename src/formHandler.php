<?php
if (empty($_POST)) exit();

require 'PHPMailer-master/PHPMailerAutoload.php';

$mail = new PHPMailer;

/*
$mail->isSMTP();
$mail->SMTPAuth = true;

$mail->Host = "smtp.mail.ru";
$mail->Port = 25;
$mail->Username = "zakaz@jbi-kf.ru";
$mail->Password = "SanAndreas123";

$mail->SMTPDebug = 3;
*/

$mail->setFrom('zakaz@'.$_SERVER['SERVER_NAME'], 'Заказ с сайта '.$_SERVER['SERVER_NAME']);
$mail->addAddress('pafi2010@ya.ru');     // Add a recipient
$mail->isHTML(true);                     // Set email format to HTML
$mail->CharSet = "UTF-8";

$mail->Subject = 'Заявка на подбор автомобиля';

if (isset($_POST['rating'])) { 
    
    $mail->Subject = 'Новый отзыв'; 
} elseif (isset($_POST['carModel'])) {
    
    $mail->Subject = 'Заявка на подбор автомобиля'; 
}else {
    
     $mail->Subject = 'Заявка с сайта';
}

$text = '<h2>'.$mail->Subject.'</h2>';

$text .= isset($_POST['name']) ? '<h5>Имя: <b>'.htmlentities($_POST['name']).'</b></h5>' : '';
$text .= isset($_POST['tel']) ? '<h5>Телефон: <b>'.htmlentities($_POST['tel']).'</b></h5>' : '';
$text .= (isset($_POST['comment']) && $_POST['name'] != '') ? '<h5>Комментарий: <b>'.htmlentities($_POST['comment']).'</b></h5>' : '';

$text .= (isset($_POST['fb']) && $_POST['name'] != '') ? '<h5>Ссылка на соц сеть: <b>'.htmlentities($_POST['fb']).'</b></h5>' : '';
$text .= isset($_POST['rating']) ? '<h5>Рейтинг: <b>'.htmlentities($_POST['rating']).'</b></h5>' : '';

$text .= isset($_POST['city']) ? '<h5>Город: <b>'.htmlentities($_POST['city']).'</b></h5>' : '';
$text .= isset($_POST['carModel']) ? '<h5>Модель автомобиля: <b>'.htmlentities($_POST['carModel']).'</b></h5>' : '';
$text .= isset($_POST['year_from']) ? '<h5>Дата выпуска: <b>'.htmlentities($_POST['year_from']).' - '.htmlentities($_POST['year_to']).'</b></h5>' : '';
$text .= isset($_POST['year_from']) ? '<h5>Цена: <b>'.htmlentities($_POST['price_from']).' - '.htmlentities($_POST['price_to']).'</b></h5>' : '';
$text .= isset($_POST['mile_from']) ? '<h5>Пробег: <b>'.htmlentities($_POST['mile_from']).' - '.htmlentities($_POST['mile_to']).'</b></h5>' : '';
$text .= isset($_POST['hp_from']) ? '<h5>Мощность двигателя: <b>'.htmlentities($_POST['hp_from']).' - '.htmlentities($_POST['hp_to']).'</b></h5>' : '';
$text .= isset($_POST['state']) ? '<h5>Состояние: <b>'.htmlentities($_POST['state']).'</b></h5>' : '';
$text .= isset($_POST['body']) ? '<h5>Кузов: <b>'.htmlentities($_POST['body']).'</b></h5>' : '';
$text .= isset($_POST['engine']) ? '<h5>Двигатель: <b>'.htmlentities($_POST['engine']).'</b></h5>' : '';
$text .= isset($_POST['transmission']) ? '<h5>Коробка: <b>'.htmlentities($_POST['transmission']).'</b></h5>' : '';
$text .= isset($_POST['drive']) ? '<h5>Привод: <b>'.htmlentities($_POST['drive']).'</b></h5>' : '';
$text .= isset($_POST['color']) ? '<h5>Цвет: <b>'.htmlentities($_POST['color']).'</b></h5>' : '';

$mail->Body = $text;

echo $text;

/*
if(!$mail->send()) {
    echo json_encode('Сообщение не было отправлено. Попробуйте еще раз <br>Mailer Error: ' . $mail->ErrorInfo);
} else {
    echo json_encode('Заявка успешно отправлена. Спасибо!');
}
*/