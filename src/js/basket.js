function getRandomName(arr){
    var rand = Math.random();
    return arr[Math.floor(rand *arr.length)];
}

function getRandSeconds(){
    var rand = Math.random()*25000;
    if((rand<8000) || (rand>29000))
    {
        return getRandSeconds();
    }else{
        return Math.floor(rand);
    }
}

var names = [
        "Hannah"
        ,"Emma"
        ,"caitlin"
        ,"Georgia"
        ,"Jessica"
        ,"Charlotte"
        ,"Sarah"
        ,"amy"
        ,"Grace"
        ,"Sophie"
        ,"Lucy"
        ,"Holly"
        ,"lily"
        ,"Chloe"
        ,"Kate"
        ,"Katie"
        ,"Emily"
        ,"Samantha"
        ,"Jess"
        ,"olivia"
        ,"Zoe"
        ,"Courtney"
        ,"Laura"
        ,"Ella"
        ,"Jade"
        ,"Rebecca"
        ,"Phoebe"
        ,"Megan"
        ,"Paige"
        ,"Shannon"
        ,"Anna"
        ,"chelsea"
        ,"Ashley"
        ,"Molly"
        ,"Victoria"
        ,"Ashleigh"
        ,"Hayley"
        ,"maddy"
        ,"Mikayla"
        ,"Ruby"
        ,"Jasmine"
        ,"Rachel"
        ,"Tara"
        ,"Heidi"
        ,"jennifer"
        ,"Maya"
        ,"Helen"
        ,"Elizabeth"
        ,"Michaela"
        ,"Lydia"
        ,"Madison"
        ,"abby"
        ,"Maddie"
        ,"Morgan"
        ,"Claudia"
        ,"Jasmin"
        ,"Aleisha"
        ,"Kat"
        ,"talia"
        ,"Kylie"
        ,"casey"
        ,"Bella"
        ,"ellie"
        ,"Jessie"
        ,"Sophia"
        ,"Rose"
        ,"Tessa"
        ,"Madeline"
        ,"Alex"
        ,"Nicole"
        ,"Maia"
        ,"stacey"
        ,"Eva"
        ,"Brianna"
        ,"Sofia"
        ,"Brearna"
        ,"Annie"
        ,"crystal"
        ,"lara"
        ,"stella"
        ,"Laila"
        ,"Kayla"
        ,"Jaimee"
        ,"Karen"
        ,"Tayla"
        ,"Aimee"
        ,"Jamie"
        ,"Katy"
        ,"Jenny"
        ,"Briana"
        ,"isabella"
        ,"Monique"
        ,"Alice"
        ,"Catherine"
        ,"Amelia"
        ,"Anastasia"
        ,"Sammy"
        ,"Harriet"
        ,"Sasha"
        ,"Izzy"
        ,"matthew"
        ,"josh"
        ,"Jacob"
        ,"Liam"
        ,"Connor"
        ,"Thomas"
        ,"James"
        ,"Jack"
        ,"Mitchell"
        ,"Ben"
        ,"jordan"
        ,"Ryan"
        ,"Hayden"
        ,"oliver"
        ,"Matt"
        ,"Brad"
        ,"Hamish"
        ,"William"
        ,"Daniel"
        ,"Cameron"
        ,"michael"
        ,"Luke"
        ,"Jake"
        ,"Noah"
        ,"John"
        ,"Caleb"
        ,"Jayden"
        ,"Sebastian"
        ,"Joshua"
        ,"shea"
        ,"Bailey"
        ,"Harry"
        ,"Nathaniel"
        ,"Corey"
        ,"gerhard"
        ,"Brodie"
        ,"Jaden"
        ,"will"
        ,"Alex"
        ,"henry"
        ,"Tofilua"
        ,"Shaun"
        ,"Tim"
        ,"Angus"
        ,"Nathan"
        ,"Aaron"
        ,"Sam"
        ,"Calum"
        ,"Campbell"
        ,"Paul"
        ,"Isaiah"
        ,"ethan"
        ,"chase"
        ,"Blake"
        ,"Leo"
        ,"Bobby"
        ,"Jim"
        ,"marc"
        ,"Troy"
        ,"Neil"
        ,"neti"
        ,'Isaac'
        ,'Edward'
        ,'Jamie'
        ,'Riley'
        ,'Andrew'
        ,'jackson'
        ,'Taine'
        ,'Louis'
        ,'Max'
        ,'Adam'
        ,'Timothy'
        ,'Hugh'
        ,'Billy'
        ,'Bruce'
        ,'Kay'
        ,'Emman'
        ,'darian'
        ,'tua'
        ,'Ronan'
        ,'Jason'
        ,'lewis'
        ,'sahil'
        ,'Austin'
        ,'Cail'
        ,'kingsnow'
        ,'Abhijit'
        ,'ludovic'
        ,'Gurtej'
        ,'Ashton'
        ,'Calvin'
        ,'Harold'
        ,'zane'
        ,'Jakob'
        ,'Allen'
        ,'Ryley'
        ,'Hugo'
        ,'Nate'
        ,'Benjamin'
    ],
    type_basket=["callback","search","plan","callback","plan","callback","search","search","callback","plan","search","callback","callback"],
    cars=[
        "Hyundai Solaris",
        "KIA Rio",
        "Renault Duster",
        "Volkswagen Polo",
        "Nissan Almera",
        "Chevrolet NIVA",
        "Renault Logan",
        "Renault Sandero",
        "Skoda Octavia",
        "Toyota RAV 4",
        "Chevrolet Cruze",
        "Hyundai ix35",
        "Ford Focus",
        "Toyota Corolla",
        "Toyota Camry",
        "KIA Sportage",
        "Opel Astra",
        "Nissan Juke",
        "Mitsubishi Outlander",
        "Nissan X-Trail",
        "Nissan Qashqai",
        "Volkswagen Tiguan",
        "Audi 100",
        "Audi A1",
        "Audi A3",
        "Audi A4",
        "Audi A5",
        "Audi A6",
        "Audi A7",
        "Audi A8",
        "Audi Q5",
        "Audi Q7",
        "Audi TT",
        "BMW 7",
        "BMW 5",
        "BMW 3",
        "BMW X5",
        "BMW X6",
        "Chevrolet Aveo",
        "Chevrolet Captiva",
        "Chevrolet Epica",
        "Chevrolet Laccetti",
        "Ford C-MAX",
        "Ford Explorer",
        "Ford Fiesta",
        "Ford Fusion",
        "Ford Kuga",
        "Ford Mondeo",
        "Honda Accord",
        "Honda Civic",
        "Honda CR-V",
        "Honda HR-V",
        "Honda Fit",
        "Honda Integra",
        "Hyundai Accent",
        "Hyundai i30",
        "Hyundai Getz",
        "Hyundai Santa Fe",
        "Hyundai Sonata",
        "Hyundai Tucson",
        "Kia Sorento",
        "Kia Optima",
        "Kia Picanto",
        "Kia Cerato",
        "Kia Cee'd",
        "Kia Soul",
        "Mazda 626",
        "Mazda Altenza",
        "Mazda Demio",
        "Mazda 3",
        "Mazda 6",
        "Mazda Famillia",
        "Mitsubishi ASX",
        "Mitsubishi Colt",
        "Mitsubishi Galant",
        "Mitsubishi Lancer",
        "Mitsubishi Pajero",
        "Nissan Murano",
        "Nissan Cefiro",
        "Nissan Juke",
        "Nissan March",
        "Nissan Tiida",
        "Nissan Sunny",
        "Nissan Note",
        "Nissan Primera",
        "Nissan Skyline",
        "Opel Astra",
        "Opel Corsa",
        "Opel Insignia",
        "Opel Vectra",
        "Subaru Forester",
        "Subaru Impreza",
        "Subaru Legacy",
        "Subaru Outback",
        "Toyota Avensis",
        "Toyota Corolla",
        "Toyota Cresta",
        "Toyota Crown",
        "Toyota Mark II"
    ],
    plans = [
        '1',
        '1',
        '1',
        '2',
        '2',
        '3'
    ];



function showUpperMsg() {
    console.log(Date.now());

    var current_plan = getRandomName(plans),
        current_car = getRandomName(cars),
        current_type = getRandomName(type_basket),
        randomName = getRandomName(names);

    if (current_type == "callback") {

        text = randomName + ' request call back';
    } else if (current_type == "search") {

        text = randomName + ' request search for ' + current_car;
    } else {

        text = randomName + ' request tariff ' + current_plan;
    }

    $('.basket__textMsg').html(text);
    $('#basket').fadeIn(1000).css("display", "flex").delay(4500).fadeOut(1000);
    /*    $('#basket .close').click(function () {
     $('#basket').fadeOut(1000);
     });*/

    /*var timerId = setTimeout('showUpperMsg()',getRandSeconds());*/



}

$(document).ready(function(e) {
    if (screen.width > 960) {

        var timerId = setTimeout(function tick() {
            showUpperMsg();
            timerId = setTimeout(tick, getRandSeconds());
        }, getRandSeconds());
    };

    $('#basket .close').click(function(){
        $('#basket').css("display", "none");
        clearTimeout(timerId);
    });
});