'use strict';
$( document ).ready(
    $( function () {

        var years = [],
            prices = [],
            miles = [],
            hp = [],
            select = {},
            lastYear = new Date().getFullYear();

        for (var i = 1970; i < lastYear+1; i++) {
            years.push({id: i, title: i})
        }

        for (var i = 5000; i < 35000; i+=5000) {
            prices.push({id: i, title: i})
        }

        for (var i = 0; i < 500000; i+=25000) {
            miles.push({id: i, title: i})
        }

        for (var i = 0; i < 500000; i+=25000) {
            miles.push({id: i, title: i})
        }

        for (var i = 50; i < 400; i+=50) {
            hp.push({id: i, title: i})
        }



        select.year_from = $('#year_from').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: years,
            onChange: function (value) {

                var slider = $( "#year_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [value,values[1]] );

            }
        });
        select.year_to = $('#year_to').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: years,
            onChange: function (value) {

                var slider = $( "#year_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [value[0],value] );

            }
        });

        select.price_from = $('#price_from').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: prices,
            onChange: function (value) {

                var slider = $( "#price_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [value,values[1]] );

            }
        });
        select.price_to = $('#price_to').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: prices,
            onChange: function (value) {

                var slider = $( "#price_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [values[0],value] );

            }
        });

        select.mile_from = $('#mile_from').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: miles,
            onChange: function (value) {

                var slider = $( "#mile_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [value,values[1]] );

            }
        });
        select.mile_to = $('#mile_to').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: miles,
            onChange: function (value) {

                var slider = $( "#mile_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [values[0],value] );

            }
        });

        select.hp_from = $('#hp_from').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: hp,
            onChange: function (value) {

                var slider = $( "#hp_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [value,values[1]] );

            }
        });
        select.hp_to = $('#hp_to').selectize({
            valueField: 'id',
            labelField: 'title',
            searchField: 'title',
            options: hp,
            onChange: function (value) {

                var slider = $( "#hp_slider" );
                var values = slider.slider( "option", "values" );
                slider.slider( "option", "values", [values[0],value] );

            }
        });

        select.state =$('#carModel').selectize({
            valueField: 'car',
            labelField: 'car',
            searchField: 'car',
            sortField: 'car',
            allowEmptyOption: true,
            options: [],
            create: false,
            /*render: {
             option: function(item, escape) {
             return '<span></span>';
             }
             },*/
            load: function(query, callback) {
                if (!query.length) return callback();

                if (query.length > 2) {
                    $.ajax({
                        url: '/getCars.html',
                        type: 'GET',
                        dataType: 'json',
                        data: {
                            query: query
                        },
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            console.log(res);
                            callback(res);
                        }
                    });
                }

            }
        })
        select.state =$('#state').selectize({
        })
        select.state =$('#body').selectize({
        })
        select.state =$('#engine').selectize({
        })
        select.state =$('#transmission').selectize({
        })
        select.state =$('#drive').selectize({
        })
        select.state =$('#color').selectize({
        })

//.slider( "option", "values", [ 2000 , 2005 ] );

        $('#year_slider').slider({
            range: true,
            min: 1970,
            max: 2017,
            values: [1970,2017],
            slide: function (e, ui) {
                select.year_from[0].selectize.setValue(ui.values[0]);
                select.year_to[0].selectize.setValue(ui.values[1]);
                console.log("Range: from "+ui.values[0]+" to "+ui.values[1]+" " );

            }
        });

        $('#price_slider').slider({
            range: true,
            min: 5000,
            max: 30000,
            step: 5000,
            values: [5000,30000],
            slide: function (e, ui) {
                select.price_from[0].selectize.setValue(ui.values[0]);
                select.price_to[0].selectize.setValue(ui.values[1]);
                console.log("Range: from "+ui.values[0]+" to "+ui.values[1]+" " );

            }
        });

        $('#mile_slider').slider({
            range: true,
            min: 0,
            max: 375000,
            step: 25000,
            values: [0,375000],
            slide: function (e, ui) {
                select.mile_from[0].selectize.setValue(ui.values[0]);
                select.mile_to[0].selectize.setValue(ui.values[1]);
                console.log("Range: from "+ui.values[0]+" to "+ui.values[1]+" " );

            }
        });

        $('#hp_slider').slider({
            range: true,
            min: 50,
            max: 350,
            step: 50,
            values: [50,350],
            slide: function (e, ui) {
                select.hp_from[0].selectize.setValue(ui.values[0]);
                select.hp_to[0].selectize.setValue(ui.values[1]);
                console.log("Range: from "+ui.values[0]+" to "+ui.values[1]+" " );

            }
        });

        select.year_from[0].selectize.setValue(years['0'].id);
        select.year_to[0].selectize.setValue(lastYear);

        select.price_from[0].selectize.setValue(prices['0'].id);
        select.price_to[0].selectize.setValue(30000);

        select.mile_from[0].selectize.setValue(miles['0'].id);
        select.mile_to[0].selectize.setValue(375000);

        select.hp_from[0].selectize.setValue(50);
        select.hp_to[0].selectize.setValue(350);


        //Прокрутка страницы
        $('a[href^="#"]').click(function(){ //берем все ссылки атрибут href которых начинается с #
            if(document.getElementById($(this).attr('href').substr(1)) != null) { //на странице есть элемент с нужным нам id
                $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500); // анимируем скролл к элементу
            }
            return false;
        });

        //Мобильное меню
        $('.header__nav-mobile').click(function () {

            $('.header').addClass('nav-open');
        });

        $('.header__nav-close').click(function () {

            $('.header').removeClass('nav-open');
        });


        //Карусель отзывов
        $(".owl-carousel").owlCarousel({
            loop:true,
            margin: 30,
            items: 1,
            dotsContainer: ".reviews__nav",
            /*animateIn: 'fadeIn',
             animateOut: 'fadeOut',*/
        });

        //Управление основной формой

        $('.btn-open').click(function () {
            $('.form_part').show();
            $(this).hide();
        });
        $('.btn-close').click(function () {
            $('.btn-open').show();
            $('.form_part').hide();
        });


        //Управление всплывающими формами
        $('button, .btn-link').on('click', function(e){

            console.log(e.target);

            if (this.getAttribute('data-action') === 'open_form') {
                e.preventDefault();
                console.log(this.getAttribute('data-form'));

                $('#'+this.getAttribute('data-form')).addClass('visible');

                if (this.getAttribute('data-plan')) {
                    console.log(this.getAttribute('data-plan'));
                }

                if (this.getAttribute('data-header')) {
                    console.log(this.getAttribute('data-header'));
                    $('#'+this.getAttribute('data-form')).find('h3').text(this.getAttribute('data-header'));
                }
            }

        });

        //Закрытие форм
        $('form').on('click', '.form_close', function (e) {

            $(this).parents('.form_callback').removeClass('visible');
        });

        //Отправка форм
        $('form').submit(function (e) {
            e.preventDefault();

            console.log(this);

            var data = {
                name: $(this).find('input[name="name"]').val(),
                tel: $(this).find('input[name="phone"]').val(),
                comment: $(this).find('textarea').val(),

                fb: $(this).find('input[name="fb"]').val(),
                rating: $(this).find('input[name="rating"]').val(),

                city: $(this).find('input[name="city"]').val(),
                carModel: $(this).find('select[name="carModel"]').val(),
                year_from: $(this).find('select[name="year_from"]').val(),
                year_to: $(this).find('select[name="year_to"]').val(),
                price_from: $(this).find('select[name="price_from"]').val(),
                price_to: $(this).find('select[name="price_to"]').val(),
                mile_from: $(this).find('select[name="mile_from"]').val(),
                mile_to: $(this).find('select[name="mile_to"]').val(),
                hp_from: $(this).find('select[name="hp_from"]').val(),
                hp_to: $(this).find('select[name="hp_to"]').val(),
                state: $(this).find('select[name="state"]').val(),
                body: $(this).find('select[name="body"]').val(),
                engine: $(this).find('select[name="engine"]').val(),
                transmission: $(this).find('select[name="transmission"]').val(),
                drive: $(this).find('select[name="drive"]').val(),
                color: $(this).find('select[name="color"]').val()

            };

            console.log(data);

            $.post( "/formHandler.php", data ,function( response ) {
                console.log(response);

                $('.form_callback').removeClass('visible');
                $('#form_thx').addClass('visible');
            });

        });


        //Обработчик рейтинга
        $('.form_rating').on('click', '.icon-star', function(){

            var stars = $(this).parent().find('.icon-star');
            var index = stars.index(this);
            $(this).parent().parent().find('input[name="rating"]').val(index+1);
            stars.addClass('empty');
            for (var i = 0; i <= index; i++) {
                $(stars[i]).removeClass('empty');
            }
        });

    }));