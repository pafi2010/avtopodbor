const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const browserSync = require('browser-sync').create();
const sourcemaps = require('gulp-sourcemaps');
const gcmq  = require('gulp-group-css-media-queries');
const uncss  = require('gulp-uncss');
const less = require('gulp-less');
const spritesmith = require('gulp.spritesmith');

var config = {
    src: './src/',
    css: {
		watch: 'less/*.less',
        src: 'less/styles.less',
        dest: 'css/',
        destLess: 'less/'
    },
	html: {
		src: '*.html'
	},
    img: {
        icons: 'img/sprite/*.png',
        dest: 'img/'
    }
};

gulp.task('build', function(){
   gulp.src(config.src + config.css.src)
	   .pipe(less())
	   .pipe(sourcemaps.init())
	   .pipe(gcmq())
	   /*uncss только для LP*/
       /*.pipe(uncss({
           //html: [config.src + config.html.src]
       }))*/
        .pipe(autoprefixer({
            browsers: ['> 1.5%'],
            cascade: false
        }))
       .pipe(cleanCSS())
	   //.pipe(sourcemaps.write('.'))
       .pipe(gulp.dest(config.src + config.css.dest))
       .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('watch', ['browserSync'], function(){
	console.log('Запущен Sync!');
    gulp.watch(config.src + config.css.watch, ['build']);
	gulp.watch(config.src + config.html.src, browserSync.reload);
});

gulp.task('browserSync', function(){
    browserSync.init({
        server: {
            baseDir:config.src
        }
    });
});

gulp.task('sprite', function () {
    var spriteData =
        gulp.src(config.src + config.img.icons) //путь, откуда берем картинки для спрайта
            .pipe(spritesmith({
                imgName: 'sprite.png',
                cssName: 'sprite.less',
                cssFormat: 'less',
                algorithm: 'binary-tree',  //binary-tree, top-down, left-right
                padding: 3,
                cssTemplate: 'less.handlebars',
                cssVarMap: function (sprite) {
                    sprite.name = 'spr-' + sprite.name
                }
            }));
    spriteData.img.pipe(gulp.dest(config.src + config.img.dest)); //путь, куда кладем спрайт
    spriteData.css.pipe(gulp.dest(config.src + config.css.destLess)); //путь, куда кладем файл стилей
});